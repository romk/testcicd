from setuptools import setup

setup(
    name='helloflask',
    version='0.1.0',
    description='Flask hello-world application',
    url='https://gitlab.com/romk/helloflask',
    license='MIT',
    author='romk',
    author_email='r0mk@gmx.net',
    install_requires=['Flask'],
    packages=['helloflask']
)
